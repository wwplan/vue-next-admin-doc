---
home: true
heroImage: https://img-blog.csdnimg.cn/9efd5420327a46b7bd6d93524a97229d.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAbHl0LXRvcA==,size_14,color_FFFFFF,t_70,g_se,x_16
heroText: " "
tagline: 基于vue3.x 、Typescript、vite、Element plus 后台开源免费模板开发文档
actionText: 开始
actionLink: /config/
altActionText: 了解更多
altActionLink: /home/
features:
  - title: 🚀 全注释
    details: 代码方法全有代码注释，方便理解、学习
  - title: 🚩 多版本
    details: 有完整版、基础版本，后期有时间将添加js版本等
  - title: 💖 解疑问
    details: 加群解答探讨开发中遇到的各种问题，1 群：665452019 2 群：766356862
  - title: 👍 全免费
    details: 后台模板永远免费，供学习、商业使用。
  - title: 🚧 维护久
    details: 只要作者不转行，将一直持续修复bug、更新迭代版本
  - title: 💯 纳建议
    details: 只要可以做的功能、页面，都尽力去做。欢迎老哥们多pr、提issues
footer: Made by lyt_20201208 with ❤️
---
