# 第三方插件使用

主目录路径：`/@/views/fun`，依赖位置：根目录 `package.json`

## countup 数字滚动

> 目录路径：`/@/views/fun/countup`，相关库、文档：[countUp.js](https://github.com/inorganik/countUp.js)

## echartsTree 树图

> 目录路径：`/@/views/fun/echartsTree` 等，相关库、文档：[echarts](https://github.com/apache/echarts)

## wangEditor 编辑器

> 目录路径：`/@/views/fun/wangEditor`，相关库、文档：[wangEditor](https://github.com/wangeditor-team/wangEditor)

## cropper 图片裁剪

> 目录路径：`/@/views/fun/cropper`，相关库、文档：[cropperjs](https://github.com/fengyuanchen/cropperjs)

## qrcode 二维码生成

> 目录路径：`/@/views/fun/qrcode`，相关库、文档：[qrcodejs](https://github.com/davidshimjs/qrcodejs)

## 地理坐标/地图

> 目录路径：`/@/views/fun/echartsMap`（根目录 index.html 中引入 `https://api.map.baidu.com/api?v=3.0&ak=wsijQt8sLXrCW71YesmispvYHitfG9gv&s=1`），相关库、文档：[百度地图开放平台](https://lbsyun.baidu.com/)

## printJs 页面打印

> 目录路径：`/@/views/fun/printJs`，相关库、文档：[printJs](https://github.com/crabbly/Print.js)

## vue-clipboard3 复制剪切

> 目录路径：`/@/views/fun/clipboard`，相关库、文档：[vue-clipboard3](https://github.com/JamieCurnow/vue-clipboard3)

## screen-shot web 端自定义截屏

> 目录路径：`/@/views/fun/screenShort`，相关库、文档：[screen-shot](https://github.com/likaia/screen-shot)

## vue-grid-layout 拖拽布局

> 目录路径：`/@/views/fun/gridLayout`，相关库、文档：[vue-grid-layout](https://github.com/jbaysolutions/vue-grid-layout)

## splitpanes 窗格拆分器

> 目录路径：`/@/views/fun/splitpanes`，相关库、文档：[splitpanes](https://github.com/antoniandre/splitpanes)

## vue-drag-verify 验证器

> 目录路径：`/@/views/fun/dragVerify`，相关库、文档：[vue-drag-verify](https://github.com/yimijianfang/vue-drag-verify)
