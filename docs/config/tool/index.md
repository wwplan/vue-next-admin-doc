# 工具类集合

> 目录结构，定期更新，目录路径：`/@/utils`，方法自行去对应的文件里查看，有注释也有调用方式说明。有不懂的，💖 欢迎您提 [Issues](https://gitee.com/lyt-top/vue-next-admin/issues)、[Pull Requests](https://gitee.com/lyt-top/vue-next-admin/pulls)。

```ts
├── src/utils
	├── arrayOperation.ts (判断两数组、两个对象是否相同)
	├── authDirective.ts (用户权限指令 app.directive)
	├── authFunction.ts (用户权限指令，用于函数调用)
	├── commonFunction.ts (通用函数基础类)
	├── customDirective.ts (自定义指令)
	├── directive.ts (自定义指令主出口)
	├── formatTime.ts (时间格式化)
	├── getStyleSheets.ts (自动获取css样式、svg 图标)
	├── loading.ts (全局loading、雪花屏)
	├── other.ts (其它公共类)
	├── request.ts (接口请求，axios)
	├── setIconfont.ts (设置icon、svg)
	├── storage.ts (设置浏览器永久、临时缓存)
	├── theme.ts (框架主题函数)
	├── toolsValidate.ts (正则工具类)
	├── viteBuild.ts (vite 打包相关)
	└── wartermark.ts (页面水印)
```
