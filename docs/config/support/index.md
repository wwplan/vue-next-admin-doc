# 支持开源

<p style="font-weight: bold;">一、参与开源项目</p>

&emsp;&emsp;使用、介绍开源软件，帮助他人使用，编撰文档，协助推广，提交 Bug（最好附上测试用例）甚至是提交功能请求，都是参与开源项目的一种方式。

&emsp;&emsp;参与开源项目的方式，不仅仅只有捐钱和捐代码这两种，对自己喜欢的开源项目，做出力所能及的贡献，即使是给项目默默地添加一个 Star，朋友圈分享自己的使用感受，都是对作者和项目的帮助。

💖 欢迎您提 [Issues](https://gitee.com/lyt-top/vue-next-admin/issues)、[Pull Requests](https://gitee.com/lyt-top/vue-next-admin/pulls)。

<p style="font-weight: bold;">二、选择捐赠</p>

<img src="https://img-blog.csdnimg.cn/79fdc41e65b54b58b6e710ac98716a8b.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAbHl0LXRvcA==,size_15,color_FFFFFF,t_70,g_se,x_16" width="30%" style="border: 1px solid var(--c-brand);">
<img src="https://img-blog.csdnimg.cn/db85818984ff475a85fff120a2e26dc1.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAbHl0LXRvcA==,size_15,color_FFFFFF,t_70,g_se,x_16" width="30%" style="border: 1px solid var(--c-brand);">

<p style="font-weight: bold;">三、捐赠名单</p>

> 感谢各位老哥的支持和捐赠

| 金额  | 捐赠者                   | 留言                                                             | 时间       | 来源   |
| ----- | ------------------------ | ---------------------------------------------------------------- | ---------- | ------ |
| 200   | \*\*利                   |                                                                  | 2022-07-16 | 支付宝 |
| 20.0  | 我打江南走               | 感谢您的开源项目！                                               | 2022-07-12 | gitee  |
| 5.00  |                          | 大佬牛逼                                                         | 2022-06-09 | 微信   |
| 66.60 | \*\*志                   | 老铁 666，太赞了                                                 | 2022-04-28 | 支付宝 |
| 50.00 | Cloud                    | 老铁 666，加油！                                                 | 2022-04-27 | 微信   |
| 50.00 | 老娄                     | 感谢您的开源项目！太赞了                                         | 2022-04-26 | 微信   |
| 66.66 | imgoddqp                 | 太牛了！加油！                                                   | 2022-04-24 | gitee  |
| 20.0  | \*\*波                   |                                                                  | 2022-04-15 | 支付宝 |
| 50.0  | 柳敏莘                   | 持续关注，虽然还没用上，有机会一定用到生产上，感谢您的开源项目！ | 2022-04-13 | gitee  |
| 1.0   | \*\*岗                   |                                                                  | 2022-04-13 | 支付宝 |
| 100.0 | \*\*波                   |                                                                  | 2022-04-07 | 支付宝 |
| 0.01  | 胡\*龙                   | 我是胡\*龙 🐉                                                    | 2022-03-11 | 微信   |
| 10.0  | \*盟                     |                                                                  | 2022-01-16 | 支付宝 |
| 2.5   | \*涛                     |                                                                  | 2021-12-21 | 支付宝 |
| 1.0   | \*尧                     | 感谢您的开源项目！好运常伴！                                     | 2021-12-16 | 支付宝 |
| 10.0  | 马格纳斯 1220            | 因为 IT 通道中人，感谢您的开源项目！                             | 2021-12-06 | gitee  |
| 10.0  | 枫\_\_\_\_\_\_\_\_\_\_\_ | 感谢您的开源项目！                                               | 2021-11-25 | gitee  |
| 20.0  | 付忠源                   | 感谢您的开源项目！                                               | 2021-10-13 | gitee  |
| 10.0  | 马浩鼎                   | 感谢您的开源项目！                                               | 2021-09-27 | gitee  |
| 10.0  | 唐参                     | 感谢您的开源项目！                                               | 2021-09-13 | gitee  |
| 10.0  | 想要起个网名真的很难     | 感谢您的开源项目！                                               | 2021-09-05 | gitee  |
| 1.0   | 小瑶瑶                   | 感谢您的开源项目！                                               | 2021-05-20 | gitee  |
