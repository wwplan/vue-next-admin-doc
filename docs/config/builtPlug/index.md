# 内置插件的使用

主目录路径：`/@/views/pages`，依赖位置：根目录 `package.json`，后续整理封装更多

## 城市多级联动

> 目录路径：`/@/views/pages/cityLinkage`，演示地址：[cityLinkage](https://lyt-top.gitee.io/vue-next-admin-preview/#/pages/cityLinkage)（需要再次封装，有时间再处理封装）

## 图标选择器

> 目录路径：`/@/views/fun/selector`，演示地址：[selector](https://lyt-top.gitee.io/vue-next-admin-preview/#/fun/selector)

## 滚动通知栏

> 目录路径：`/@/views/fun/noticeBar`，演示地址：[noticeBar](https://lyt-top.gitee.io/vue-next-admin-preview/#/fun/noticeBar)
