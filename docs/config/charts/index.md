# 数据可视化

<p style="font-weight: bold;">一、项目目录</p>

> `/@/views/visualizing`

- 演示预览 [visualizingDemo1](https://lyt-top.gitee.io/vue-next-admin-preview/#/visualizingDemo1)
- 演示预览 [visualizingDemo2](https://lyt-top.gitee.io/vue-next-admin-preview/#/visualizingDemo2)

<p style="font-weight: bold;">二、图表截图</p>

> 查看大图，鼠标右键：`在新标签页中打开图片`。后续有时间将添加更多大数据图表演示例子。

<img src="https://gitee.com/lyt-top/vue-next-admin-images/raw/master/docs/chart1.png" width="50%" style="border: 1px solid var(--c-brand);">
<img src="https://gitee.com/lyt-top/vue-next-admin-images/raw/master/docs/chart2.png" width="50%" style="border: 1px solid var(--c-brand);">
